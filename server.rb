require 'curses'
require 'socket'

GOPHER_HOST  = 'localhost'
GOPHER_PORT  = 7070
SCREEN_WIDTH = 80

def format(type, description, selector)
  fields = ["#{type}#{description}", selector, GOPHER_HOST, GOPHER_PORT]
  fields.join("\t")
end

def list_all(client)
  arr = Dir.glob('*').sort_by(&:downcase)
  dirs  = arr.select { |f| File.directory?(f) }
  files = arr - dirs

  resources = [['Directories', dirs, '1'],
               ['Files', files, '0']]
  resources.each do |description, resource_arr, type|
    resource_arr.each do |filename|
      client.puts format(type, filename, filename)
    end
  end
end

server = TCPServer.open(7070)
loop {
  Thread.start(server.accept) do |client|
    cmd = client.gets
    list_all(client) if(cmd == "\n")

    client.puts '.'
    client.close
  end
}
