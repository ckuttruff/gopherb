# coding: utf-8
class Curses::Item
  attr_accessor :selector
end

class Gopherb
  class Ncurses
    def self.init
      Curses.init_screen
      Curses.cbreak
      Curses.noecho
      Curses.stdscr.keypad = true

      at_exit do
        Curses.close_screen
      end
    end

    def self.reset
      Curses.clear
      Curses.cbreak
      Curses.noecho
      Curses.refresh
    end

    def self.bottom
      Curses.lines - 2
    end

    def self.prompt
      Curses.setpos(bottom, 0)
      Curses.addstr(': ')
      Curses.nocbreak
      Curses.echo
      Curses.getstr.strip
    end

    # parsed_lines: array of hashes
    #   eg: [{:description=>"blog", :selector=>"blog", :type=>:directory}]
    def self.create_menu(parsed_lines)
      lines = []
      parsed_lines.each do |h|
        emoji = (h[:type] == :directory) ? '📁' : '📃'
        desc  = "#{emoji} - #{h[:description]}"
        lines << h.merge(menu_description: desc)
      end

      items = lines.map { |h| Curses::Item.new(h[:menu_description], '') }
      menu = Curses::Menu.new(items)
      menu.post
      menu
    end

  end
end
